import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import HomeScreen from '../screens/HomeScreen'
import DetailsScreen from '../screens/DetailsScreen'


const Stack = createStackNavigator()

export default class MainStack extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Hola desde home' }} />
                <Stack.Screen name="Details" component={DetailsScreen} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}