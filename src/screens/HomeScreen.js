import React from 'react'
import { Text, ImageBackground, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Container, Content, View, Button, Card, CardItem } from "native-base"

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props)
    }

    render () {

        const image = require('../../assets/bgImage.jpg')
        const calendar = { uri: 'https://freeiconshop.com/wp-content/uploads/edd/calendar-flat.png' }
        const assistant = { uri: 'https://icons-for-free.com/iconfiles/png/512/assistant-131964752347025887.png' }
        const medicalServices = { uri: 'https://cdn.iconscout.com/icon/free/png-256/medical-127-129383.png' }
        const bio = { uri: 'https://image.flaticon.com/icons/png/512/1497/1497835.png' }
        const contacts = { uri: 'https://i.pinimg.com/originals/68/5c/ce/685cceffa93afa89416c4345481bf834.png' }

        return (
            <View style={styles.container}>
                <ImageBackground source={image} style={styles.image}>
                    <View
                      style={{
                          flexDirection: 'row',
                          paddingHorizontal: 50
                      }}
                    >
                        <Card style={{ flex: 1 }}>
                              <TouchableOpacity>
                                <CardItem cardBody style={{ height: 150, justifyContent: 'center' }}>
                                    <Image source={calendar} style={{height: 120, width: 120 }}/>
                                </CardItem>
                              </TouchableOpacity>
                        </Card>

                        <Card style={{ flex: 1 }}>
                            <CardItem cardBody style={{ height: 150, justifyContent: 'center' }}>
                                <Image source={assistant} style={{height: 120, width: 120 }}/>
                            </CardItem>
                        </Card>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            paddingHorizontal: 50
                        }}
                      >
                          <Card style={{ flex: 1 }}>
                              <CardItem cardBody style={{ height: 150, justifyContent: 'center' }}>
                                  <Image source={medicalServices} style={{height: 120, width: 120 }}/>
                              </CardItem>
                          </Card>
                          <Card style={{ flex: 1 }}>
                              <CardItem cardBody style={{ height: 150, justifyContent: 'center' }}>
                                  <Image source={bio} style={{height: 120, width: 120 }}/>
                              </CardItem>
                          </Card>
                      </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            paddingHorizontal: 50
                        }}
                      >
                          <Card style={{ flex: 1 }}>
                              <CardItem cardBody style={{ height: 150, justifyContent: 'center' }}>
                                  <Image source={contacts} style={{height: 120, width: 120 }}/>
                              </CardItem>
                          </Card>
                          <View style={{ flex: 1 }} />
                      </View>
                </ImageBackground>

                <View
                  style={{
                    position: 'absolute',
                    bottom: 15,
                    right: 15,
                    width: 100,
                    height: 100
                  }}
                >
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                    <Button danger rounded style={{ alignSelf: 'center', paddingHorizontal: 20, paddingVertical: 40 }}>
                      <Text style={{ color: 'white' }}>Pánico</Text>
                    </Button>
                  </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "grey",
    fontSize: 30,
    fontWeight: "bold"
  }
});

